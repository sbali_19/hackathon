package com.hackathon.stockservice.service;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.fail;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.hackathon.stockservice.controller.dto.StockDto;
import com.hackathon.stockservice.model.Stock;
import com.hackathon.stockservice.repository.StockServiceRepository;

@ExtendWith(MockitoExtension.class)
public class StockServiceTest {
	@Mock
	private StockServiceRepository stockServiceRepository;
	@Mock
	private ObjectMapper objectMapper;

	@InjectMocks
	private StockServiceImpl stockServiceImpl;

	@Test
	@DisplayName("addina new stock")
	@Order(1)
	public void testSaveStock() {
		StockDto stockDto = new StockDto("HCL", 1000.0);
		Stock stock = new Stock(1l, stockDto.getStockName(), stockDto.getStockPrice());
	
		when(objectMapper.convertValue(stockDto, Stock.class)).thenReturn(stock);
		when(stockServiceRepository.save(stock)).thenReturn(stock);
		String responseMessage = stockServiceImpl.saveStock(stockDto);
		if (!responseMessage.contains("HCL")) {
			fail();
		}

	}

	@Test
	@DisplayName("get all stocks")
	@Order(2)
	public void testGetAllStocks() {
		StockDto stockDto = new StockDto("HCL", 1000.0);
		Stock stock = new Stock(1l, stockDto.getStockName(), stockDto.getStockPrice());
		List<Stock> stocks = new ArrayList<Stock>();
		stocks.add(stock);
		Page<Stock> pageStocks = new PageImpl<Stock>(stocks);
		Pageable pageable = PageRequest.of(0, 1);

		when(stockServiceRepository.findAll(pageable)).thenReturn(pageStocks);
		List<Stock> stock1 = stockServiceImpl.getAllStocks(pageable);
		assertEquals(stocks.size(), stock1.size());

	}

	@Test
	@DisplayName("Get Stock By StockName")
	@Order(3)
	public void testGetStockByStockName() {
		StockDto stockDto = new StockDto("HCL", 1000.0);
		Stock stock = new Stock(1l, stockDto.getStockName(), stockDto.getStockPrice());
		when(stockServiceRepository.findByStockName(stockDto.getStockName())).thenReturn(Optional.of(stock));
		when(objectMapper.convertValue(stock, StockDto.class)).thenReturn(stockDto);

		StockDto stockDto2 = stockServiceImpl.getStockByStockName(stockDto.getStockName());
		assertEquals(stockDto2.getStockName(), stockDto.getStockName());
	}
}
