package com.hackathon.stockservice.controller;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockHttpServletRequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.hackathon.stockservice.controller.dto.StockDto;
import com.hackathon.stockservice.service.StockService;

@WebMvcTest(controllers = StockServiceController.class)
public class StockServiceControllerTest {
     @MockBean
	private StockService stockService;
	@Autowired
   private ObjectMapper objectMapper;
   @Autowired
   private MockMvc mockMvc;
	@Test
	@DisplayName("add stock test")
	@Order(1)
	public void addStockTest() throws Exception {

		
		StockDto stockDto=new StockDto("HCL", 1000.0);
		MockHttpServletRequestBuilder request = MockMvcRequestBuilders.post("/stocks")
				.contentType(MediaType.APPLICATION_JSON)
				.content(objectMapper.writeValueAsString(stockDto));

		MvcResult result = mockMvc.perform(request).andReturn();

		MockHttpServletResponse response = result.getResponse();

		assertEquals(HttpStatus.CREATED.value(), response.getStatus());
	}

	@Test
	@DisplayName("get all Stocks")
	@Order(2)
	public void getAllStocks() throws Exception {

		
		
		MockHttpServletRequestBuilder request = MockMvcRequestBuilders.get("/stocks");
				

		MvcResult result = mockMvc.perform(request).andReturn();

		MockHttpServletResponse response = result.getResponse();

		assertEquals(HttpStatus.OK.value(), response.getStatus());
	}
	
	@Test
	@DisplayName("get Stock By Stock Name")
	@Order(3)
	public void getStockByStockName() throws Exception {

		
		
		MockHttpServletRequestBuilder request = MockMvcRequestBuilders.get("/stocks/{stockName}","hcl");
				

		MvcResult result = mockMvc.perform(request).andReturn();

		MockHttpServletResponse response = result.getResponse();

		assertEquals(HttpStatus.OK.value(), response.getStatus());
	}
	
	@Test
	@DisplayName("update stock price value")
	@Order(4)
	public void updateStockPriceValue() throws Exception {

		
		StockDto stockDto=new StockDto("HCL", 100.0);
		MockHttpServletRequestBuilder request = MockMvcRequestBuilders.put("/stocks")
				.contentType(MediaType.APPLICATION_JSON)
				.content(objectMapper.writeValueAsString(stockDto));

		MvcResult result = mockMvc.perform(request).andReturn();

		MockHttpServletResponse response = result.getResponse();

		assertEquals(HttpStatus.OK.value(), response.getStatus());
	}

}
