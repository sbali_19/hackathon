package com.hackathon.stockservice.repository;

import java.util.Optional;

import org.springframework.data.repository.PagingAndSortingRepository;


import com.hackathon.stockservice.model.Stock;

public interface StockServiceRepository extends PagingAndSortingRepository<Stock, Long> {

   public Optional<Stock> findByStockName(String stockName);
  

}
