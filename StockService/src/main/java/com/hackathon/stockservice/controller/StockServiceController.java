package com.hackathon.stockservice.controller;

import java.util.List;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.hackathon.stockservice.controller.dto.StockDto;
import com.hackathon.stockservice.model.Stock;
import com.hackathon.stockservice.service.StockService;

@RestController
@RequestMapping("/stocks")
public class StockServiceController {

	@Autowired
	StockService stockService;

	private static final Logger log = LoggerFactory.getLogger(StockServiceController.class);

	// Adding new stocks to stock service
	@PostMapping
	public ResponseEntity<String> addNewStock(@Valid @RequestBody StockDto stockDto) {
		log.info("adding new stocks to stock service");
		
		return new ResponseEntity<>(stockService.saveStock(stockDto), HttpStatus.CREATED);
	}

	// list of all stocks
	@GetMapping
	public ResponseEntity<List<Stock>> getAllStocks(
			@RequestParam(required = false, defaultValue = "0", value = "pageId") Integer pageId,
			@RequestParam(required = false, defaultValue = "4", value = "pageSize") Integer pageSize,
			@RequestParam(required = false, defaultValue = "stockName", value = "columnName") String columnName) {

		
		log.info("getting list of all stocks");
		Pageable pageable = PageRequest.of(pageId, pageSize, Sort.by(columnName).ascending());
		List<Stock> stocks = stockService.getAllStocks(pageable);

		return new ResponseEntity<>(stocks, HttpStatus.OK);
	}

	// getting stock by stockname
	@GetMapping("/{stockName}")
	public ResponseEntity<StockDto> getStockByStockName(@PathVariable("stockName") String stockName) {

		
		log.info("getting stock by stock name");
		return new ResponseEntity<>(stockService.getStockByStockName(stockName), HttpStatus.OK);
	}

	
	//updating the price of a stock
	@PutMapping
	public ResponseEntity<String> changeInPriceOfStock(@Valid @RequestBody StockDto stockDto) {
	
		log.info("updating the price of a stock");
		return new ResponseEntity<>(stockService.changeInPriceOfStock(stockDto), HttpStatus.OK);
	}

}
