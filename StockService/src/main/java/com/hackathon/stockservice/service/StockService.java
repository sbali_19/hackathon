package com.hackathon.stockservice.service;

import java.util.List;

import javax.validation.Valid;

import org.springframework.data.domain.Pageable;

import com.hackathon.stockservice.controller.dto.StockDto;
import com.hackathon.stockservice.model.Stock;

public interface StockService {

	String saveStock( StockDto stockDto);

	List<Stock> getAllStocks(Pageable pageable);

	StockDto getStockByStockName(String stockName);

	String changeInPriceOfStock(@Valid StockDto stockDto);

}
