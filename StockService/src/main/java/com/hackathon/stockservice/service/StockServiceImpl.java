package com.hackathon.stockservice.service;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.hackathon.stockservice.controller.dto.StockDto;
import com.hackathon.stockservice.exception.StockNotFoundException;
import com.hackathon.stockservice.model.Stock;
import com.hackathon.stockservice.repository.StockServiceRepository;

@Service
public class StockServiceImpl implements StockService {

	@Autowired
	private StockServiceRepository stockServiceRepository;

	@Autowired
	private ObjectMapper objectMapper;
	
	
	private static final Logger log = LoggerFactory.getLogger(StockServiceImpl.class);


	//adding new stock 
	@Override
	public String saveStock(StockDto stockDto) {
		
		log.info("adding new stock-service class");

		Stock stock = objectMapper.convertValue(stockDto, Stock.class);

		return stockServiceRepository.save(stock).getStockName() + " inserted successfully";
	}

	
	//getting all stocks
	@Override
	public List<Stock> getAllStocks(Pageable pageable) {

		log.info("getting all stocks - service class");
		Page<Stock> stocksPages = stockServiceRepository.findAll(pageable);

		if (stocksPages.hasContent()) {
			return stocksPages.getContent();
		}
		return new ArrayList<Stock>();
	}

	
	//getting stock by stock name
	@Override
	public StockDto getStockByStockName(String stockName) {

		log.info("getting stock by stock name - service class");
		Stock stock = stockServiceRepository.findByStockName(stockName)
				.orElseThrow(() -> new StockNotFoundException("Stock  is not exist"));
		return objectMapper.convertValue(stock, StockDto.class);
	}

	//updating the price of a stock
	@Override
	public String changeInPriceOfStock(StockDto stockDto) {
		
		log.info("upadting changed price of a stock-service class");
		Stock stock = stockServiceRepository.findByStockName(stockDto.getStockName())
				.orElseThrow(() -> new StockNotFoundException("Stock  is not exist"));
		stock.setStockPrice(stockDto.getStockPrice());
		return stockServiceRepository.save(stock).getStockName() + " updated successfully";
	}

}
