package com.hackathon.portfoliomanagement.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class RequestOrderDetailsDto {
	
	private Long orderId;
	private String stockName;
	private Double price;

	private Long customerId;
	private Long accountId;

}
