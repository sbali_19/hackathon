package com.hackathon.portfoliomanagement.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * 
 * @author satya
 *
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class OrderDetailsDto {
	private Long orderId;
	private String stockName;
	private Double price;

}
