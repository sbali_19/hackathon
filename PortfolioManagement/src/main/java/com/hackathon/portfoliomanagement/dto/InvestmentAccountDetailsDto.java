package com.hackathon.portfoliomanagement.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

 

/**
 * 
 * @author rakesh
 *
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class InvestmentAccountDetailsDto {
    private Long investmentAccountNo;
    private Double balance;

 

}
