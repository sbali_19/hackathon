package com.hackathon.portfoliomanagement.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

 

/**
 * 
 * @author satya
 *
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class PortfolioDto {
    private Long investmentAccountNo;
    private Double balance;
    private String stockName;
    private Double price;
    private int quantity;
    private Double totalPortfolioValue;

 

}
 
