package com.hackathon.portfoliomanagement.model;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name="Investment_Account")
public class InvestmentAccount{
    @Id
    @Column
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    private Long investmentAccountId;
  
    @ManyToOne(cascade = CascadeType.ALL)
    @JoinColumn(name="customer_Id")
    private Customer customer;

    @Column(name = "account_number")
    private Long accountNumber;

    @Column(name = "balance")
    private Double balance;

    @OneToMany(mappedBy = "investmentAccount",cascade = CascadeType.ALL)
    private List<OrderDetails> orderDetails;

}
 
