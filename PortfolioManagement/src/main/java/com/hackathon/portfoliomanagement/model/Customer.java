package com.hackathon.portfoliomanagement.model;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name="customer")
public class Customer {
    @Id
   
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    private Long customerId;
    
    private String customerName;

    private String password;
    
    @OneToMany(mappedBy = "customer",cascade = CascadeType.ALL)
    private List<InvestmentAccount> investmentAccounts;
    
    @OneToMany(mappedBy = "customerOrder")
    private List<OrderDetails> orderDetails;

 


}
