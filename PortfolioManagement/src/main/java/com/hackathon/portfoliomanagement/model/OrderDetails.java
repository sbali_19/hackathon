package com.hackathon.portfoliomanagement.model;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "order_details")
public class OrderDetails {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long orderId;
	
	private String stockName;
	
	private Integer stockQuantity;
	
	private Double stockPrice;
	
	@Temporal(TemporalType.TIMESTAMP)
	private Date orderedDate;
	
	@ManyToOne
	@JoinColumn(name = "customer_id")
	private Customer customerOrder;
	
	@ManyToOne
	@JoinColumn(name="investment_account_id")
	private InvestmentAccount investmentAccount;
	
	
	
	
}
