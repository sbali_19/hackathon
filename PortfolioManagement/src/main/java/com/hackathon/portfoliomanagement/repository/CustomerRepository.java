package com.hackathon.portfoliomanagement.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.hackathon.portfoliomanagement.model.Customer;

@Repository
public interface CustomerRepository extends CrudRepository<Customer, Long> {

}
