package com.hackathon.portfoliomanagement.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import com.hackathon.portfoliomanagement.model.Customer;
import com.hackathon.portfoliomanagement.model.InvestmentAccount;
import com.hackathon.portfoliomanagement.model.OrderDetails;

@Repository
public interface OrderDetailsRepository extends PagingAndSortingRepository<OrderDetails, Long>  {

	Page<OrderDetails> findByCustomerOrderAndInvestmentAccount(Customer customer,
			InvestmentAccount investmentAccount, Pageable pageable);

}
