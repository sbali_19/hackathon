package com.hackathon.portfoliomanagement.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import com.hackathon.portfoliomanagement.model.Customer;
import com.hackathon.portfoliomanagement.model.InvestmentAccount;

@Repository
public interface InvestmentAccountRepository extends PagingAndSortingRepository<InvestmentAccount, Long>{

	 Page<InvestmentAccount> findByCustomer(Customer customer, Pageable pageable);

}
