package com.hackathon.portfoliomanagement.controller;

import java.util.List;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.hackathon.portfoliomanagement.dto.InvestmentAccountDetailsDto;
import com.hackathon.portfoliomanagement.dto.LoginDto;
import com.hackathon.portfoliomanagement.dto.OrderDetailsDto;
import com.hackathon.portfoliomanagement.dto.PortfolioDto;
import com.hackathon.portfoliomanagement.dto.RequestOrderDetailsDto;
import com.hackathon.portfoliomanagement.model.OrderDetails;
import com.hackathon.portfoliomanagement.service.CustomerService;
import com.hackathon.portfoliomanagement.service.InvestmentAccountService;
import com.hackathon.portfoliomanagement.service.OrderService;

@RestController
@RequestMapping("/customers")
public class PortfolioManagementController {

	@Autowired
	CustomerService customerService;
	
	@Autowired
	InvestmentAccountService investmentAccountService;
	
	@Autowired
	OrderService orderService;
	
	private static final Logger logger = LoggerFactory.getLogger(PortfolioManagementController.class);

	@PostMapping("/login")
	public ResponseEntity<String> customerLogin(@RequestBody LoginDto loginDto) {
		return new ResponseEntity<String>(customerService.checkLogin(loginDto), HttpStatus.OK);
	}

	/**
	 * Fetch all order details based on customer id and investment id
	 * @author satya 
	 * @param customerId
	 * @param investmentaccountId
	 * @param pageId
	 * @param pageSize
	 * @param columnName
	 * @return
	 */
	@GetMapping("{customerId}/investmentaccounts/{investmentaccountId}/orderdetails")
	public ResponseEntity<List<OrderDetailsDto>> getAllOrderDetails(@PathVariable("customerId") Long customerId,
			@PathVariable("investmentaccountId") Long investmentaccountId,
			@RequestParam(required = false, defaultValue = "0", value = "pageId") Integer pageId,
			@RequestParam(required = false, defaultValue = "5", value = "pageSize") Integer pageSize,
			@RequestParam(required = false, defaultValue = "orderedDate", value = "columnName") String columnName) {
		logger.info("getAllOrderDetails() started..");
		Pageable pageable = PageRequest.of(pageId, pageSize, Sort.by(columnName).ascending());
		return new ResponseEntity<>(
				investmentAccountService.getAllOrderDetails(customerId, investmentaccountId, pageable), HttpStatus.OK);

	}
	
	
	
	
	@PostMapping("/orderdetails")
	public ResponseEntity<OrderDetails> orderDetails(@Valid @RequestBody RequestOrderDetailsDto requestOrderDetailsDto){
		logger.info("Call PortfolioManagementController orderDetails API");
		return new ResponseEntity<OrderDetails>(orderService.saveOrderDetails(requestOrderDetailsDto),HttpStatus.OK);
		
	}
	
	@GetMapping(value = "/{customerId}/investmentAccounts")
    public ResponseEntity<List<InvestmentAccountDetailsDto>> getInvestmentAccount(@PathVariable Long customerId,
            @RequestParam(required = false, defaultValue = "0", value = "pageId") Integer pageId,
            @RequestParam(required = false, defaultValue = "5", value = "pageSize") Integer pageSize,
            @RequestParam(required = false, defaultValue = "investmentAccountId", value = "columnName") String columnName) {
        Pageable pageable = PageRequest.of(pageId, pageSize, Sort.by(columnName).ascending());
        return new ResponseEntity<>(
                investmentAccountService.getAllInvestmentAccount(customerId, pageable),
                HttpStatus.OK);

 

    }

 

    @GetMapping("{customerId}/investmentaccounts/{investmentaccountId}/portfolio")
    public ResponseEntity<List<PortfolioDto>> getPortfolio(@PathVariable("customerId") Long customerId,
            @PathVariable("investmentaccountId") Long investmentaccountId,
            @RequestParam(required = false, defaultValue = "0", value = "pageId") Integer pageId,
            @RequestParam(required = false, defaultValue = "5", value = "pageSize") Integer pageSize) {
        Pageable pageable = PageRequest.of(pageId, pageSize);
        return new ResponseEntity<>(
                investmentAccountService.getPortfolio(customerId, investmentaccountId, pageable),
                HttpStatus.OK);
    }
}

