package com.hackathon.portfoliomanagement.vo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class StockVo {
     
    private String stockName;
	
	private Double stockPrice;
}

