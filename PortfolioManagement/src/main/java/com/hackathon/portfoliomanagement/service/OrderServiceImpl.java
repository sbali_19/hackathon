package com.hackathon.portfoliomanagement.service;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.hackathon.portfoliomanagement.constant.AppConstants;
import com.hackathon.portfoliomanagement.controller.PortfolioManagementController;
import com.hackathon.portfoliomanagement.dto.RequestOrderDetailsDto;
import com.hackathon.portfoliomanagement.exception.CustomerNotFoundException;
import com.hackathon.portfoliomanagement.exception.InvestmentAccountNotFoundException;
import com.hackathon.portfoliomanagement.model.Customer;
import com.hackathon.portfoliomanagement.model.InvestmentAccount;
import com.hackathon.portfoliomanagement.model.OrderDetails;
import com.hackathon.portfoliomanagement.repository.CustomerRepository;
import com.hackathon.portfoliomanagement.repository.InvestmentAccountRepository;
import com.hackathon.portfoliomanagement.repository.OrderDetailsRepository;
import com.hackathon.portfoliomanagement.vo.StockVo;

@Service
public class OrderServiceImpl implements OrderService {

	@Autowired
	private OrderDetailsRepository  orderDetailsRepository;
	@Autowired
	private CustomerRepository customerRepository;
	@Autowired
	private InvestmentAccountRepository investmentAccountRepository;
	
	
	private static final Logger logger = LoggerFactory.getLogger(PortfolioManagementController.class);
	
	@SuppressWarnings("unused")
	@Override
	public OrderDetails saveOrderDetails(@Valid RequestOrderDetailsDto requestOrderDetailsDto) {
		logger.info("start OrderServiceImpl ");
		
		Long customerId = requestOrderDetailsDto.getCustomerId();
		Long investmentaccountId = requestOrderDetailsDto.getAccountId();
		Customer customer = customerRepository.findById(customerId)
				.orElseThrow(() -> new CustomerNotFoundException(AppConstants.CUSTOMER_ID +" - "+ customerId + AppConstants.NOT_FOUND));
		InvestmentAccount investmentAccount = investmentAccountRepository.findById(investmentaccountId)
				.orElseThrow(() -> new InvestmentAccountNotFoundException(AppConstants.INVESTMENT_ID +" - "+investmentaccountId+ AppConstants.NOT_FOUND));
		
		Long investmentAccountId = requestOrderDetailsDto.getAccountId();
		OrderDetails orderDetails = null;
			RestTemplate restTemplate = new RestTemplate(); 
			String url = "http://localhost:8090/stocks/"+requestOrderDetailsDto.getStockName(); 
			logger.info("call to stock api with url "+url);
			StockVo stockVo = restTemplate.getForObject(url, StockVo.class); 
			if(stockVo.getStockName()!= null) {
				logger.info("stock is exist in DB end OrderServiceImpl ");
				//TODO:Need to check and set all the property
				//requestOrderDetailsDto.setStockPrice(stockVo.getStockPrice());
				orderDetails= new OrderDetails();
				orderDetails.setOrderId(requestOrderDetailsDto.getOrderId());
				orderDetails.setStockName(requestOrderDetailsDto.getStockName());
				orderDetails.setStockPrice(stockVo.getStockPrice());
				//orderDetails.setCustomerOrder(getCustomer());
			}
			else {
				//TODO: Need to check
				return null;
			}
			return  orderDetailsRepository.save(orderDetails);
		
	}

	/**
	 * @author satya
	 * get order details based on customer and investment account
	 */
		public Page<OrderDetails> findByCustomerOrderAndInvestmentAccount(Customer customer,
				InvestmentAccount investmentAccount, Pageable pageable) {
			return orderDetailsRepository.findByCustomerOrderAndInvestmentAccount(customer, investmentAccount, pageable);
		}

	}
