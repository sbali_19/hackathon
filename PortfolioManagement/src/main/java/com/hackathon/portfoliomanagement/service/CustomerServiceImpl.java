package com.hackathon.portfoliomanagement.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.hackathon.portfoliomanagement.dto.LoginDto;
import com.hackathon.portfoliomanagement.exception.CustomerNotFoundException;
import com.hackathon.portfoliomanagement.repository.CustomerRepository;

@Service
public class CustomerServiceImpl implements CustomerService{ 

	@Autowired
	private CustomerRepository customerRepository;

	public String checkLogin(LoginDto loginDto) {
		return customerRepository.findById(loginDto.getCustomerId()).map((customer) -> {
			if (customer.getPassword().equals(loginDto.getPassword())) {
				return "Customer login successfull";
			}
			return "Incorrect username or password";

		}).orElseThrow(() -> new CustomerNotFoundException("Customer not registered"));

	}
}
