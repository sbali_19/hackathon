package com.hackathon.portfoliomanagement.service;

import java.util.List;

import org.springframework.data.domain.Pageable;

import com.hackathon.portfoliomanagement.dto.InvestmentAccountDetailsDto;
import com.hackathon.portfoliomanagement.dto.OrderDetailsDto;
import com.hackathon.portfoliomanagement.dto.PortfolioDto;

public interface InvestmentAccountService {

	List<OrderDetailsDto> getAllOrderDetails(Long customerId, Long investmentaccountId, Pageable pageable);

	List<InvestmentAccountDetailsDto> getAllInvestmentAccount(Long customerId, Pageable pageable);

	List<PortfolioDto> getPortfolio(Long customerId, Long investmentaccountId, Pageable pageable);

}
