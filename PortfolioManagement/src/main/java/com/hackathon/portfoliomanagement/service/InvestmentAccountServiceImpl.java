package com.hackathon.portfoliomanagement.service;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.hackathon.portfoliomanagement.constant.AppConstants;
import com.hackathon.portfoliomanagement.dto.InvestmentAccountDetailsDto;
import com.hackathon.portfoliomanagement.dto.OrderDetailsDto;
import com.hackathon.portfoliomanagement.dto.PortfolioDto;
import com.hackathon.portfoliomanagement.exception.CustomerNotFoundException;
import com.hackathon.portfoliomanagement.exception.InvestmentAccountNotFoundException;
import com.hackathon.portfoliomanagement.model.Customer;
import com.hackathon.portfoliomanagement.model.InvestmentAccount;
import com.hackathon.portfoliomanagement.model.OrderDetails;
import com.hackathon.portfoliomanagement.repository.CustomerRepository;
import com.hackathon.portfoliomanagement.repository.InvestmentAccountRepository;

@Service
public class InvestmentAccountServiceImpl implements InvestmentAccountService {

	private Logger logger = LoggerFactory.getLogger(this.getClass());

	@Autowired
	CustomerRepository customerRepository;

	@Autowired
	InvestmentAccountRepository investmentAccountRepository;

	@Autowired
	OrderService orderService;

	/**
	 * 
	 */
	public List<OrderDetailsDto> getAllOrderDetails(Long customerId, Long investmentaccountId, Pageable pageable) {
		logger.info("getAllOrderDetails() started in InvestmentAccountServiceImpl.class");
		Customer customer = customerRepository.findById(customerId).orElseThrow(() -> new CustomerNotFoundException(
				AppConstants.CUSTOMER_ID + " - " + customerId + AppConstants.NOT_FOUND));
		InvestmentAccount investmentAccount = investmentAccountRepository.findById(investmentaccountId)
				.orElseThrow(() -> new InvestmentAccountNotFoundException(
						AppConstants.INVESTMENT_ID + " - " + investmentaccountId + AppConstants.NOT_FOUND));

		Page<OrderDetails> pageOders = orderService.findByCustomerOrderAndInvestmentAccount(customer, investmentAccount,
				pageable);

		return pageOders.hasContent() ? convertOrderDetailsDto(pageOders.getContent()) : new ArrayList<>();
	}

	/**
	 * 
	 * @param ordersList
	 * @return
	 */
	public List<OrderDetailsDto> convertOrderDetailsDto(List<OrderDetails> ordersList) {
		logger.info("convertOrderDetailsDto() started in InvestmentAccountServiceImpl.class");
		return ordersList.stream()
				.map(order -> new OrderDetailsDto(order.getOrderId(), order.getStockName(), order.getStockPrice()))
				.collect(Collectors.toList());
	}

	/**
     * this method will fetch all investment account for a particular customer
     * 
     */
    @Override
    public List<InvestmentAccountDetailsDto> getAllInvestmentAccount(Long customerId, Pageable pageable) {
        Customer customer = customerRepository.findById(customerId).orElseThrow(() -> new CustomerNotFoundException(
                AppConstants.CUSTOMER_ID + " - " + customerId + AppConstants.NOT_FOUND));
        Page<InvestmentAccount> pageOders = investmentAccountRepository.findByCustomer(customer, pageable);
        return pageOders.hasContent() ? convertInvestmentAccountDetailsDto(pageOders.getContent()) : new ArrayList<>();
    }

 

    private List<InvestmentAccountDetailsDto> convertInvestmentAccountDetailsDto(
            List<InvestmentAccount> investmentAccountDetails) {
        return investmentAccountDetails.stream()
                .map(investmentAccount -> new InvestmentAccountDetailsDto(investmentAccount.getAccountNumber(),
                        investmentAccount.getBalance()))
                .collect(Collectors.toList());
    }

 

    /**
     * this method will fetch all investment account for a particular customer
     * 
     */
    @Override
    public List<PortfolioDto> getPortfolio(Long customerId, Long investmentaccountId, Pageable pageable) {
        Customer customer = customerRepository.findById(customerId).orElseThrow(() -> new CustomerNotFoundException(
                AppConstants.CUSTOMER_ID + " - " + customerId + AppConstants.NOT_FOUND));
        InvestmentAccount investmentAccount = investmentAccountRepository.findById(investmentaccountId)
                .orElseThrow(() -> new InvestmentAccountNotFoundException(
                        AppConstants.INVESTMENT_ID + " - " + investmentaccountId + AppConstants.NOT_FOUND));
        Page<OrderDetails> pageOders = orderService.findByCustomerOrderAndInvestmentAccount(customer, investmentAccount,
                pageable);
        return pageOders.hasContent() ? convertPortfolioDto(pageOders.getContent()) : new ArrayList<>();
    }

 

    public  List<PortfolioDto> convertPortfolioDto(List<OrderDetails> portfolioDetails) {
        return portfolioDetails.stream()
                .map(portfolioDetail -> new PortfolioDto(portfolioDetail.getInvestmentAccount().getAccountNumber(),
                        portfolioDetail.getInvestmentAccount().getBalance(), portfolioDetail.getStockName(),
                        portfolioDetail.getStockPrice(), portfolioDetail.getStockQuantity(),
                        portfolioDetail.getStockQuantity() * portfolioDetail.getStockPrice()))
                .collect(Collectors.toList());
    }
}
