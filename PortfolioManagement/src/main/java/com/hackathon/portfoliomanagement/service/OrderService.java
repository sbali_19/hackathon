package com.hackathon.portfoliomanagement.service;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import com.hackathon.portfoliomanagement.dto.RequestOrderDetailsDto;
import com.hackathon.portfoliomanagement.model.Customer;
import com.hackathon.portfoliomanagement.model.InvestmentAccount;
import com.hackathon.portfoliomanagement.model.OrderDetails;

public interface OrderService {

	OrderDetails saveOrderDetails(RequestOrderDetailsDto requestOrderDetailsDto);

	Page<OrderDetails> findByCustomerOrderAndInvestmentAccount(Customer customer, InvestmentAccount investmentAccount,
			Pageable pageable);

	

}
