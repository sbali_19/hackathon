package com.hackathon.portfoliomanagement.service;

import com.hackathon.portfoliomanagement.dto.LoginDto;


public interface CustomerService {

	String checkLogin(LoginDto loginDto);

}
