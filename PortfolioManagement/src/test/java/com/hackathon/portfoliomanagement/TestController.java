package com.hackathon.portfoliomanagement;


import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertNull;

import java.util.Date;

import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockHttpServletRequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.hackathon.portfoliomanagement.model.Customer;
import com.hackathon.portfoliomanagement.model.InvestmentAccount;
import com.hackathon.portfoliomanagement.model.OrderDetails;
import com.hackathon.portfoliomanagement.service.InvestmentAccountService;
import com.hackathon.portfoliomanagement.service.OrderService;

@WebMvcTest(controllers = TestController.class)
public class TestController {
    
	@MockBean
	private InvestmentAccountService investmentAccountService;
	
	@MockBean
	private OrderService orderService;
	
	@Autowired
	private ObjectMapper objectMapper;
	
	@Autowired
	private MockMvc mockMvc;
	
	@Test
	@DisplayName("Portfolio Management Test ")
	@Order(1)
	public void orderDetails() throws Exception {
		
		OrderDetails OrderDetails=getOrderDetails();
		MockHttpServletRequestBuilder request = MockMvcRequestBuilders.post("/orderdetails")
				.contentType(MediaType.APPLICATION_JSON)
				.content(objectMapper.writeValueAsString(OrderDetails));
		MvcResult result = mockMvc.perform(request).andReturn();
		MockHttpServletResponse response = result.getResponse();
		assertNotNull(response);
	}
	
	

	static Customer getCustomer() {
		Customer customer= new Customer();
		customer.setCustomerId(1L);
		customer.setCustomerName("sunil");
		customer.setInvestmentAccounts(null);
		customer.setOrderDetails(null);
		return customer;
		}
	static InvestmentAccount getInvestmentAccount() {
		InvestmentAccount investmentAccount= new InvestmentAccount();
		investmentAccount.setAccountNumber(1L);
		investmentAccount.setBalance(10000000.3);
		investmentAccount.setCustomer(getCustomer());
		investmentAccount.setInvestmentAccountId(12L);
		investmentAccount.setOrderDetails(null);
		return investmentAccount;
		
	}
	
	static OrderDetails getOrderDetails() {
		OrderDetails orderDetails = new OrderDetails();
		orderDetails.setOrderId(100L);
		orderDetails.setStockName("hcl");
		orderDetails.setStockPrice(10000.0);
		orderDetails.setStockQuantity(10);
		orderDetails.setOrderedDate(new Date());
		orderDetails.setInvestmentAccount(getInvestmentAccount());
		orderDetails.setCustomerOrder(getCustomer());
		return orderDetails;
		}
	}