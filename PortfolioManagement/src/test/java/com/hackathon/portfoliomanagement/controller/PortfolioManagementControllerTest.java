package com.hackathon.portfoliomanagement.controller;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.HttpStatus;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockHttpServletRequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import com.hackathon.portfoliomanagement.service.CustomerService;
import com.hackathon.portfoliomanagement.service.InvestmentAccountService;
import com.hackathon.portfoliomanagement.service.OrderService;

@WebMvcTest(controllers = PortfolioManagementController.class)
public class PortfolioManagementControllerTest {

	@Autowired
	private MockMvc mockMvc;
	
	@MockBean
	CustomerService customerService;
	
	@MockBean
	InvestmentAccountService investmentAccountService;
	
	@MockBean
	OrderService orderService;
	
	@Test
	@DisplayName("Get All Order Details")
	public void testGetAllOrderDetails() throws Exception {
		
		MockHttpServletRequestBuilder request = MockMvcRequestBuilders.get("/customers/1/investmentaccounts/1/orderdetails");

		MvcResult result = mockMvc.perform(request).andReturn();

		MockHttpServletResponse response = result.getResponse();

		assertEquals(HttpStatus.OK.value(), response.getStatus());
		
	}
}
