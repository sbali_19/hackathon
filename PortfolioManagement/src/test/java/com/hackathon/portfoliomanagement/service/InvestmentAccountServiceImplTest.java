package com.hackathon.portfoliomanagement.service;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.ArrayList;
import java.util.List;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;

import com.hackathon.portfoliomanagement.dto.OrderDetailsDto;
import com.hackathon.portfoliomanagement.model.OrderDetails;
import com.hackathon.portfoliomanagement.repository.CustomerRepository;
import com.hackathon.portfoliomanagement.repository.InvestmentAccountRepository;
import com.hackathon.portfoliomanagement.repository.OrderDetailsRepository;

@ExtendWith(MockitoExtension.class)
public class InvestmentAccountServiceImplTest {

	@Autowired
	OrderService orderService;

	@Mock
	CustomerRepository customerRepository;

	@Mock
	InvestmentAccountRepository investmentAccountRepository;

	@Mock
	OrderDetailsRepository orderDetailsRepository;

	InvestmentAccountServiceImpl investmentAccountService = new InvestmentAccountServiceImpl();;

	@Test
	public void testConvertOrderDetailsDto() {

		List<OrderDetails> ordersList = new ArrayList<>();
		OrderDetails order = new OrderDetails();
		order.setOrderId(1L);
		order.setStockName("HCL Tech");
		order.setStockPrice(100.00);
		ordersList.add(order);
		ordersList.add(new OrderDetails(2L, "Tesla", 100, null, null, null, null));

		List<OrderDetailsDto> orderDetailsDtos = investmentAccountService.convertOrderDetailsDto(ordersList);
		assertEquals(ordersList.size(), orderDetailsDtos.size());
		assertEquals(ordersList.get(0).getOrderId(), orderDetailsDtos.get(0).getOrderId());
		assertEquals(ordersList.get(0).getStockName(), orderDetailsDtos.get(0).getStockName());
		assertEquals(ordersList.get(0).getStockPrice(), orderDetailsDtos.get(0).getPrice());
	}

	
}
